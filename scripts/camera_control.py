from time import time
from time import sleep
from picamera import PiCamera
import yaml

camera = PiCamera()


def get_config():
    with open("config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
    return config


config = get_config()

while True:
    camera.capture("{}/{}.jpg".format(config["timelapse_directory"], time()))
    sleep(config["timelapse_seconds"])

