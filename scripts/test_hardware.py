import RPi.GPIO as GPIO
import time
import sys
import yaml

def get_config():
    with open("config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
    return config

config = get_config()
GPIO.setmode(GPIO.BCM)
t_start = time.time()

print('########## CONFIG ###########')
for i in config.items():
    print('{}: {}'.format(i[0],i[1]))
print('#############################')
print('Enabling fan...')

try:
    GPIO.setup(config["fan_pin"], GPIO.OUT)
    GPIO.output(config["fan_pin"], GPIO.HIGH)
    print('Success! Fan should be spinning.')
except Exception as e:
    print('Fan pin: {}'.format(config["fan_pin"]))
    print('When trying to enable the fan pin got exception {}'.format(e))
    print('Exiting.  Please check the config and try again.')
    sys.exit(1)

print('Enabling humidifer...')

try:
    GPIO.setup(config["fog_pin"], GPIO.OUT)
    GPIO.output(config["fog_pin"], GPIO.HIGH)
    print('Success! The humidifier should be fogging.')
except Exception as e:
    print('Fog pin: {}'.format(config["fog_pin"]))
    print('When trying to enable the fog pin got exception {}'.format(e))
    print('Exiting.  Please check the config and try again.')
    sys.exit(1)

print('Enabling light...')

try:
    GPIO.setup(config["light_pin"], GPIO.OUT)
    GPIO.output(config["light_pin"], GPIO.HIGH)
    print('Success! The light should be on.')
except Exception as e:
    print('Light pin: {}'.format(config["light_pin"]))
    print('When trying to enable the light pin got exception {}'.format(e))
    print('Exiting.  Please check the config and try again.')
    sys.exit(1)

while True:
    if(time.time() - t_start >= 10):
        print('Test complete. Powering down all hardware.')
        GPIO.output(config["light_pin"], GPIO.LOW)
        GPIO.output(config["fog_pin"], GPIO.LOW)
        GPIO.output(config["fan_pin"], GPIO.LOW)
        GPIO.cleanup()
        sys.exit(0)
    else:
        time.sleep(1)
        print('{} seconds remaining'.format(int(11 - (time.time()-t_start))))