import RPi.GPIO as GPIO
from subprocess import Popen, PIPE, STDOUT
import time
import re
import sys
import logging
import signal
from time import sleep
import yaml

def get_config():
    with open("config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
    return config

def read_humidity(config, inValue):
    cmd = "{}/Adafruit_Python_DHT/examples/AdafruitDHT.py {} {}".format(
        config["DHT_home"], config["sensor_type"], config["sensor_gpio_pin"]
    )
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.stdout.read()

    a = re.findall(r"Humidity=\d+", output.decode("utf-8"))
    try:
        return int(a[0][-2:])
    except IndexError:
        return inValue

def handler(signum, config, frame):
    print("Got kill signal...")
    GPIO.output(config["fog_pin"], GPIO.LOW)
    GPIO.cleanup()

config = get_config()

GPIO.setmode(GPIO.BCM)
GPIO.setup(config["fog_pin"], GPIO.OUT)
humidity = 50  # Need *some* value to use in case the sensor is dead

logging.basicConfig(
    filename="/home/pi/terrarium/scripts/fogger.log",
    filemode="w",
    format="%(asctime)s - %(message)s",
    level=logging.INFO,
)
k = 0
t_earlier = 0
while True:
    signal.signal(signal.SIGTERM, handler)
    humidity = read_humidity(humidity, config)
    if humidity < 65:
        if (time.time() - t_earlier) > config["fog_wait_time"]:
            GPIO.output(config["fog_pin"], GPIO.HIGH)
            k += 1
            logging.info("Humidity: {}.  Fogger count: {}".format(humidity, k))
            time.sleep(10)
            GPIO.output(config["fog_pin"], GPIO.LOW)
            t_earlier = time.time()
    sleep(0.1)
