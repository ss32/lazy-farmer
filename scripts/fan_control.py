import RPi.GPIO as GPIO
import time
import sys
import signal
from time import sleep
import yaml

def get_config():
    with open("config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
    return config

def handler(signum, config, frame):
    GPIO.output(config["fan_pin"], GPIO.LOW)
    GPIO.cleanup()

config = get_config()

GPIO.setmode(GPIO.BCM)
GPIO.setup(config["fan_pin"], GPIO.OUT)

t_earlier = 0
t_fan = 0
while True:
    signal.signal(signal.SIGTERM, handler)
    signal.signal(signal.SIGINT, handler)
    if (time.time() - t_earlier) > config["fan_cyle_time"]:
        GPIO.output(config["fan_pin"], GPIO.HIGH)
        t_fan = time.time()
        t_earlier = time.time()
    if (time.time() - t_fan) > config["fan_run_time"]:
        GPIO.output(config["fan_pin"], GPIO.LOW)
        t_fan = time.time()
    sleep(0.1)
