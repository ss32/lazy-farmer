#!/bin/bash

if [ $EUID -ne 0 ]; then
	echo "$0 is not running as root.  Please run with sudo."
	exit 2
fi

install_service_file(){
	if [ ! -f "$3" ]; then
		echo "Installing $3"
		ln -s $1/service_files/$3 $2/$3
	fi
}

SYSTEMD="/etc/systemd/system"

install_service_file $(pwd) $SYSTEMD fogger.service
systemctl enable fogger

install_service_file $(pwd) $SYSTEMD fan.service
systemctl enable fan

install_service_file $(pwd) $SYSTEMD light.service
systemctl enable light
echo ""
echo "Services enabled!"
