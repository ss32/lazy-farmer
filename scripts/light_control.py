import RPi.GPIO as GPIO
import time
import signal
from time import sleep
import yaml

def get_config():
    with open("config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
    return config


config = get_config()

GPIO.setmode(GPIO.BCM)
GPIO.setup(config["light_pin"], GPIO.OUT)


def handler(signum, frame):
    GPIO.output(config["light_pin"], GPIO.LOW)
    GPIO.cleanup()


t_earlier = 0
while True:
    signal.signal(signal.SIGTERM, handler)
    hour_now = time.localtime(time.time()).tm_hour
    if hour_now >= config["hour_light_on"] and hour_now <= config["hour_light_off"]:
        if (time.time() - t_earlier) > 300:
            GPIO.output(config["light_pin"], GPIO.HIGH)
            t_earlier = time.time()
    else:
        if (time.time() - t_earlier) > 300:
            GPIO.output(config["light_pin"], GPIO.LOW)
            t_earlier = time.time()
    sleep(0.1)
