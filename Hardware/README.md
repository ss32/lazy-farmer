## Hardware Build

---

### Light

Nothing special is required here, just attach the light to the lid or near the top of the inside of the container.  I used a 3M command strip to attach it to the lid, but one of the long sides is probably a better option as the light has a tendency to fall off if the lid is anything but perfectly flat.

---

### Air Exchange
1. Print the [fan plate](./120mm_fan_filter_bracket.stl).
2. On a short side of the container, trace out the inside of the fan plate and mark the screw holes.
   * Don't place it all the way at the bottom.  Leave room for the perlite/grow media.
3. Using a dremel, hot knife, or tool of your choice, cut the square out.
4. Drill out the screw holes
5. Remove the fan's plastic wire harness to expose the pins inside if possible, or cut it off and strip some of the insulation, exposing the wires.
6. Plug the wires into the barrel jack adapter (see the picture below)
   * Now is a good time to plug in the 12V adapter and make sure the fan is spinning the correct direction.
7. Place the fan plate on the inside of the container and align the fan on the outside.
    * *Optional*: Cut a 105mm square from the filter material and place it in betwteen the fan and the plate before attaching the fan
8. Using the M3 bolts/nuts, attach the fan.
   * Plug in the 12V adapter to the fan and make sure that air is being pushed into the container.  Once you've verified that the fan is working correctly, set the power supply aside.


**Fan on the outside of the container with Lion's Mane growing inside**

![Fan outside](../images/fan_outside.jpg)

**Fan filter on the inside of the container.  Note the room left below the fan for the perlite.**

![Fan inside](../images/fan_inside.jpg)


**Fan wires plugged in to the 12v barrel connector**

![Fan power](../images/fan_power.jpg)

---

### Power Cables

Instead of splicing the power cables for the pieces of hardware, I chose to prepare extension cables to work with the relay board and then plug the hardware into these cables.

1. Slice down the middle of the cable to seprate the two strands of wire.
   * An Xacto knife works really well here
2. Cut one strand of wire and remove a small amount of insulation from both sides.
3. Add solder the exposed copper strands to make them one "solid" wire.
4. Bend the cord and zip-tie it to keep the exposed cables parallel to each other.

![Power Cables](../images/power_cables.jpg)

---

### Humidifier

The 3D printed parts aren't absolutely necessary here.  If you have an easy way of routing the mist from the humidifier, use that instead.

1. Print the [humidifier adapter](./humidifier_adapter.stl)
2. Attach the vinyl tube to the adapter using a hose clamp
3. Cut a 1.5in hole in the lid of the container
4. Run the other end of the hose into the hole
   * *Coming Soon:* Part files for a threaded hose adapter that provides a tighter fit between the hose and the container lid.

**Hose adapter attached to the humidifer**

![Humidifer Adapater](../images/humidifer_adapter.jpg)

