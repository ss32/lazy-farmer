# Lazy-Farmer

Scripts, systemd service files, and 3D-printable parts to automate a terrarium with a Raspberry Pi.  Built as a sort of backend for [TerrariumPI](https://github.com/theyosh/TerrariumPI).

## Overview

The saying goes "necessity is the mother of invention", but in my experience that couldn't be farther from the truth.  The true impetus for invention is sloth; why would I do something myself when I can build a machine to do it for me?  Or better yet, why build a machine when I can buy a cheap computer and write code to automate the work?  To that end, I wrote these scripts to automate the task of minding a terrarium for growing culinary mushrooms.  Mushrooms need the same things most life requires: air, water, and light, and the code in here controls just that.

   * **Air**: Fresh air exchange is controlled via a 120mm computer fan
   * **Water**: Humidity is supplied from a cheap  ultrasonic humidifier, attached with vinyl tube and some 3D printed adapters
   * **Light**: A small fluorescent light on a timer supplies light

All of the hardware is configured in a single YAML file and controlled using a relay hat for the Raspberry Pi.  STL files and FreeCAD documents are provided for all 3D-printable parts.

*Not automated but included:* A simple timelapse script that will save frames to a directory.

---

### BOM

 * 1x [64 quart or larger latching storage container](https://www.homedepot.com/p/Sterilite-64-Qt-Latching-Storage-Box-14978006/206721480)
 * 1x [Raspberry Pi](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/?resellerType=home)
   * This is a Model 3 B+ but any of them will work.  I use a Model 2B.
   * If you are using an older model or a standard Pi Zero you will need a wifi adapter
 * 1x [Relay Board](https://www.electronics-salon.com/products/electronics-salon-rpi-power-relay-board-expansion-module-for-raspberry-pi-a-b-2b-3b)
 * 1x [DHT-22 temperature & humidity sensor](https://www.adafruit.com/product/385)
 * 1x [Air Innovations MH-408 Humidifier](https://www.air-innovations.com/cool-mist-humidifier-for-medium-rooms-70-hour-run-time-mh-408)
   * Any humidifer will work, but the adapter I designed is meant for this model
 * 3ft [0.625in inner diameter vinyl tubing](https://www.homedepot.com/p/UDP-5-8-in-I-D-x-3-4-in-O-D-x-10-ft-Clear-Vinyl-Tubing-T10006012/304185144)
 * 1x [0.5-1.25in hose clamp](https://www.homedepot.com/p/Everbilt-1-2-1-1-4-in-Stainless-Steel-Hose-Clamp-6712595/202309385)
 * 3x [6ft extension cords](https://www.homedepot.com/p/Southwire-6-ft-16-2-White-Household-Cube-Tap-Extension-Cord-94118901/314376188)
   * Three foot cords work just as well
 * 1x [GE Slim Line 14in Fluorescent Light](https://www.homedepot.com/p/GE-Slim-Line-14-in-Fluorescent-Under-Cabinet-Light-Fixture-with-5-ft-Cord-10168/202024436)
 * 1x [2-Pin 120mm fan](https://www.amazon.com/USlingbi-3000RPM-Radiator-Cooling-Computer/dp/B0822VK93D/ref=sr_1_2?dchild=1&keywords=120mm+fan&qid=1606664663&refinements=p_n_feature_keywords_three_browse-bin%3A5462061011%2Cp_36%3A1253503011&rnid=386442011&s=pc&sr=1-2)
 * 1x [12v Power Supply](https://www.amazon.com/100-240V-Transformers-Switching-Applications-Connectors/dp/B077PW5JC3/ref=sr_1_3?crid=W0NHVCNQFVXX&dchild=1&keywords=12v+power+supply&qid=1606664775&s=electronics&sprefix=12v+power+s%2Celectronics%2C167&sr=1-3)
 * 4x 35mm or longer M3 bolt and nuts used to secure the fan.
   * If you're really lazy you could glue the fan to the container
   * Cheap assorted M3 kits can be found on Ebay or Amazon
 * Optional [fan filter material](https://www.homedepot.com/p/Vent-Filter-12-pack-PFVENT-12/313965576)

---

## Prerequisite Installations

The installation assumes that you have installed [TerrariumPI](https://github.com/theyosh/TerrariumPI), but can be configured to run without it as well. The only other requirements are regex, [PyYaml](https://pyyaml.org/), and [RPi.GPIO](https://pypi.org/project/RPi.GPIO/) which can be installed via the requirements file with

```
python3 -m pip install -r requirements.txt --user
```

**Note:** If you do not want to use TerrariumPI, clone the [Adafruit DHT library](https://github.com/adafruit/DHT-sensor-library) to your home directory.

---

## Configuration

Anything that might need to be tweaked can be found in the [configuration file](./scripts/config.yaml), along  with full explanations of the variables.  

| Variable          | Default Value          | Description                                                   |
|-------------------|------------------------|---------------------------------------------------------------|
| sensor_gpio_pin   | 4                      | GPIO pin of the sensor                                        |
| sensor_type       | 22                     | 11 or 22, for DHT-11 or DHT-22                                |
| DHT_home          | /home/pi/TerrariumPI | Location of Adafruit_Python_DHT library                       |
| fan_pin           | 20                     | Pin number on relay board                                     |
| fan_cycle_time    | 1300                   | Time in seconds to wait before running the fan                |
| fan_run_time      | 60                     | Time in seconds to run the fan                                |
| light_pin         | 21                     | Pin number on relay board                                     |
| hour_light_on     | 6                      | Hour of the day to turn the light on                          |
| hour_light_off    | 17                     | Hour of the day to turn the light off                         |
| fog_pin           | 26                     | Pin number on relay board                                     |
| fog_time          | 10                     | Time in seconds to run the humidifier                         |
| minimum_humidity  | 65                     | Humidity readying that will trigger the humidifier to turn on |
| fog_wait_time     | 300                    | How long to wait (seconds) before checking the humidity value |
| timelapse_seconds | 300                    | Time in seconds to wait between frames                        |
| timelapse_directory | /home/pi/timelapse | Directory where images will be saved



---

## Assembly, Testing, and Installation

Before starting, make sure you have prepared the hardware per the [hardware README](./Hardware/README.md).

### Wire the Relays

Relays can be wired normally-open (NO) or normally-closed (NC), depending on the desired default behavior.  All of the relays here will be wired NO so by default nothing will be powered on until the GPIO pin is set high.

![Relay closeup](./images/relay_closeup.jpg)

Wire each extension cable into the C (common) and NC terminals.  Since this is AC power, it does not matter which soldered end of the extension cable goes into which terminal, only that both ends are wired to the same relay. 

![Wire closeup](./images/wire_closeup.jpg)

Label the extension cords for the the piece of harware that will be plugged into it.  The relay board is attached to GPIO pins 26, 20, and 21 for channels 1, 2, and 3 respectively. These pins can be configured in the yaml file, but the default setup has the humidifer on channel 1, the fan on channel 2, and light on channel 3.  The `pin:channel` mapping can be found on the board, and individual channels can be disabled by removing the corresponding jumper.

![Relay pins](./images/relay_pins.jpg)

### Install the Sensor

Run the positive lead to a 5V pin, the negative lead to a ground pin, and the lead labelled "out" to GPIO pin 4 (physical pin 7).  See www.pinout.xyz for more information on the Raspberry Pi's GPIO pins.

Assuming that the Adafruit library lives in `/home/pi/TerrariumPI` you can test the sensor with

```
sudo python3 /home/pi/TerrariumPI/Adafruit_Python_DHT/examples/AdafruitDHT.py 22 4
```
and should see a response like

```
Temp=18.6*  Humidity=64.3%
```

### Testing

Plug in the fan, light, and humidifier.  Log in to the Pi and execute the following command to test the hardware.

```
sudo python3 /home/pi/lazy-farmer/scripts/test_hardware.py
```

You should hear the relays switching power over and everything should power on for 10 seconds before turning off.  

### Installation

The service files make two assumptions:

  1. You are the `pi` user
  2. This directory exists at `/home/pi/lazy-farmer`

If either of those assumptions is false, edit the service files before installing.  The relevant portion to edit is the path to each of the control scripts, as these are executed as services at boot.

Install the services with 

```
sudo ./install_service_files.sh 
```
You will now have control over the fan, light, and humidifier via `systemd` and everything will start at boot.  The names of the services are `fan`, `light`, and `fogger` and you can start, stop, and query the status like any other service on the Pi.

```
pi@raspberrypi:~ $ service light status
● light.service - Light Control
   Loaded: loaded (/home/pi/terrarium/service_files/light.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2020-11-18 07:03:36 CST; 1 weeks 4 days ago
 Main PID: 493 (python3)
    Tasks: 1 (limit: 1939)
   CGroup: /system.slice/light.service
           └─493 /usr/bin/python3 /home/pi/terrarium/scripts/light_control.py

Nov 18 07:03:36 raspberrypi systemd[1]: Started Light Control.
```
